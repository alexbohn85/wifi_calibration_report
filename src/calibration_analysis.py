from __future__ import division
from __future__ import print_function
from os import walk
import os
from reports import *

# only for debug
import collections



#----------------#
# Implementation #
#----------------#

DEBUG = False

# Report ID patters
PASS_PATTERN = "  ##        #########       ##       ##"
FAIL_PATTERN = "  ##       #########  ##  ##"
VALID_RUN_21dBm = "5825        t7          1      21.0"
VALID_RUN_22dBm = "5825        t7          1      22.0"
PATTERN_EVM_FAIL = "   -28.0   fail"


def debug_list_check(debug_list_pass = [], debug_list_fail =[]):
	c = collections.Counter(debug_list_pass)
	for item in debug_list_pass:
		print("%d : %s" %(c[item], item))


def safe_div(x, y):
	if y == 0: return 0
	return x/y


def get_current_folder_filelist():
	"""Get list of reports filename in the current folder """
	filelist_temp = []
	filelist = []
	for (dirpath, dirnames, filenames) in walk(os.getcwd()):
		filelist_temp.extend(filenames)
		break
	for filename in filelist_temp:
		name, ext = os.path.splitext(filename)
		if "report" in name:
			if ".txt" in ext:
				filelist.append(filename)
	print (" - Reports found...\t %d reports" %len(filelist))
	return filelist


def cleanup():
	cleanup_filelist = []
	for (dirpath, dirnames, filenames) in walk(os.getcwd()):
		cleanup_filelist.extend(filenames)
		break
	for filename in cleanup_filelist:
		if "onlyfails.txt" in filename:
			os.remove(filename)
		elif "print_out.txt" in filename:
			os.remove(filename)
		elif "ch1.txt" in filename:
			os.remove(filename)
		elif "ch2.txt" in filename:
			os.remove(filename)


def initiate():
	cleanup()
	filelist = get_current_folder_filelist()		
	return filelist


def build_list_of_reports(filelist = []):
	
	maclist = []
	reportlist = []	

	print (" - Building list... \t", end=" ")
	i = 0
	for filename in filelist:		
		board_filelist = []	
		# Filename exemple: 0728_160958_58108C6122AE_20170728_160958_report.txt	
		mac = filename[12:-27]		
		# Skip duplicated MAC
		if mac not in maclist:			
			i += 1
			temp = "%d boards with a valid report found" %i
			print (temp, end="")
			maclist.append(mac)

			# Collect same MAC filenames
			for filename2 in filelist:
				if mac in filename2:
					board_filelist.append(filename2)
			report = Report(board_filelist, False)
			reportlist.append(report)
			print ("\b" * len(temp), end="")
	print ("%d boards with a valid report found" % len(maclist))

	reports = Reports(reportlist, maclist)
	return reports


def run(reports = []):
	
	stats = Statistics()
	stat_num_report_files = len(reports.reportlist)
	stat_num_discarted = 0
	pass_rate = 0
	fail_rate = 0

	debug_list_pass = []
	debug_list_fail = []

	i = 0
	print (" - Analyzing reports.. \t", end=" ")
	for report in reports.reportlist:
		report.isValid = False	
		i += 1
		temp = "%d reports analyzed" %i
		print (temp, end="")
		fd_fails = open("onlyfails.txt", "a+")

		for filename in report.board_filelist:
			f = open(filename, "r")
			lines = f.readlines()
			f.close()

			for text in lines:
				if VALID_RUN_21dBm in text:																
					report.isValid = True				
					break
				if VALID_RUN_22dBm in text:
					report.isValid = True
					break

			if report.isValid == False:
				stat_num_discarted += 1				

			if report.isValid == True:
				# Put all fail lines into one huge file
				for text in lines:
					if PATTERN_EVM_FAIL in text:
						fd_fails.write(text)

				# Evaluate all PASS and FAIL lines
				for text in lines:		
					if PASS_PATTERN in text:	
						pass_rate += 1
						if DEBUG is True:
							debug_list_pass.append(filename)
						report.pass_reports.append(filename)						
						break
					elif FAIL_PATTERN in text:
						fail_rate += 1
						if DEBUG is True:
							debug_list_fail.append(filename)
						report.fail_reports.append(filename)
						break
					elif text == lines[-1]:
						stat_num_discarted += 1
						reports.invalid_reports.append(filename)

		print ("\b"*len(temp), end="")
		fd_fails.close()
	if DEBUG is True:
		debug_list_check(debug_list_pass, debug_list_fail)
	print ("%d boards analyzed!" %len(reports.reportlist))
	stats.set_fail_rate(fail_rate)
	stats.set_pass_rate(pass_rate)
	stats.set_num_report_files(stat_num_report_files)
	stats.set_num_discarted(stat_num_discarted)	

	return stats, reports


def split_into_files():
	
	fd = open("onlyfails.txt", "a+")
	lines = fd.readlines()
	fd.close()
	total = len(lines)
	i = 0
	print (" - Generating files... \t", end=" ")
	for text in lines:		
		i += 1
		p = (i*100/total)		
		test = "processing %3.2f%%" %p
		print (test, end="")

		if   "5180        t7          1" in text:
			fd = open("5180ch1.txt", "a+")
			fd.write(text)
			fd.close()
			print ("\b"*len(test), end="")
		elif "5180        t7          2" in text:
			fd = open("5180ch2.txt", "a+")
			fd.write(text)
			fd.close()
			print ("\b"*len(test), end="")
		elif "5260        t7          1" in text:
			fd = open("5260ch1.txt", "a+")
			fd.write(text)
			fd.close()
			print ("\b"*len(test), end="")
		elif "5260        t7          2" in text:
			fd = open("5260ch2.txt", "a+")
			fd.write(text)
			fd.close()
			print ("\b"*len(test), end="")
		elif "5320        t7          1" in text:
			fd = open("5320ch1.txt", "a+")
			fd.write(text)
			fd.close()
			print ("\b"*len(test), end="")
		elif "5320        t7          2" in text:
			fd = open("5320ch2.txt", "a+")
			fd.write(text)
			fd.close()
			print ("\b"*len(test), end="")
		elif "5500        t7          1" in text:
			fd = open("5500ch1.txt", "a+")
			fd.write(text)
			fd.close()
			print ("\b"*len(test), end="")
		elif "5500        t7          2" in text:
			fd = open("5500ch2.txt", "a+")
			fd.write(text)
			fd.close()
			print ("\b"*len(test), end="")
		elif "5600        t7          1" in text:
			fd = open("5600ch1.txt", "a+")
			fd.write(text)
			fd.close()
			print ("\b"*len(test), end="")
		elif "5600        t7          2" in text:
			fd = open("5600ch2.txt", "a+")
			fd.write(text)
			fd.close()
			print ("\b"*len(test), end="")
		elif "5700        t7          1" in text:
			fd = open("5700ch1.txt", "a+")
			fd.write(text)
			fd.close()
			print ("\b"*len(test), end="")
		elif "5700        t7          2" in text:
			fd = open("5700ch2.txt", "a+")
			fd.write(text)
			fd.close()
			print ("\b"*len(test), end="")
		elif "5785        t7          1" in text:
			fd = open("5785ch1.txt", "a+")
			fd.write(text)
			fd.close()
			print ("\b"*len(test), end="")
		elif "5785        t7          2" in text:
			fd = open("5785ch2.txt", "a+")
			fd.write(text)
			fd.close()
			print ("\b"*len(test), end="")
		elif "5825        t7          1" in text:
			fd = open("5825ch1.txt", "a+")
			fd.write(text)
			fd.close()
			print ("\b"*len(test), end="")
		elif "5825        t7          2" in text:
			fd = open("5825ch2.txt", "a+")
			fd.write(text)
			fd.close()
			print ("\b"*len(test), end="")

	print ("processing 100.00%")


def statistic_fails_per_freq_n_chain():

	fd = open("onlyfails.txt", "r")
	lines = fd.readlines()
	fd.close()

	# Fail Counters
	c5180_1 = 0
	c5180_2 = 0
	c5260_1 = 0
	c5260_2 = 0
	c5320_1 = 0
	c5320_2 = 0
	c5500_1 = 0
	c5500_2 = 0
	c5600_1 = 0
	c5600_2 = 0
	c5700_1 = 0
	c5700_2 = 0
	c5785_1 = 0
	c5785_2 = 0
	c5825_1 = 0
	c5825_2 = 0

	for text in lines:
		if "5180        t7          1" in text:
			c5180_1 += 1
		elif "5180        t7          2" in text:
			c5180_2 += 1
		elif "5260        t7          1" in text:
			c5260_1 += 1
		elif "5260        t7          2" in text:
			c5260_2 += 1
		elif "5320        t7          1" in text:
			c5320_1 += 1
		elif "5320        t7          2" in text:
			c5320_2 += 1
		elif "5500        t7          1" in text:
			c5500_1 += 1
		elif "5500        t7          2" in text:
			c5500_2 += 1
		elif "5600        t7          1" in text:
			c5600_1 += 1
		elif "5600        t7          2" in text:
			c5600_2 += 1
		elif "5700        t7          1" in text:
			c5700_1 += 1
		elif "5700        t7          2" in text:
			c5700_2 += 1
		elif "5785        t7          1" in text:
			c5785_1 += 1
		elif "5785        t7          2" in text:
			c5785_2 += 1
		elif "5825        t7          1" in text:
			c5825_1 += 1
		elif "5825        t7          2" in text:
			c5825_2 += 1

	total_chain1 = c5180_1 + c5260_1 + c5320_1 + c5500_1 + c5600_1 + c5700_1 + c5785_1 + c5825_1
	total_chain2 = c5180_2 + c5260_2 + c5320_2 + c5500_2 + c5600_2 + c5700_2 + c5785_2 + c5825_2

	# print result
	print("")
	print(" ----------------------")
	print("     Chain 1 Fails     ")
	print(" ----------------------")
	print("  freq.\t\tperc.")
	print("  5180: %4d\t%3d %%" %(c5180_1, safe_div(c5180_1, total_chain1)*100))
	print("  5260: %4d\t%3d %%" %(c5260_1, safe_div(c5260_1, total_chain1)*100))
	print("  5320: %4d\t%3d %%" %(c5320_1, safe_div(c5320_1, total_chain1)*100))
	print("  5500: %4d\t%3d %%" %(c5500_1, safe_div(c5500_1, total_chain1)*100))
	print("  5600: %4d\t%3d %%" %(c5600_1, safe_div(c5600_1, total_chain1)*100))
	print("  5700: %4d\t%3d %%" %(c5700_1, safe_div(c5700_1, total_chain1)*100))
	print("  5785: %4d\t%3d %%" %(c5785_1, safe_div(c5785_1, total_chain1)*100))
	print("  5852: %4d\t%3d %%" %(c5825_1, safe_div(c5825_1, total_chain1)*100))
	print(" ----------------------")
	print(" Total: %6d" %(total_chain1))
	print("")
	print(" ----------------------")
	print("     Chain 2 Fails     ")
	print(" ----------------------")
	print("  freq.\t\tperc.")
	print("  5180: %4d\t%3d %%" %(c5180_2, safe_div(c5180_2, total_chain2)*100))
	print("  5260: %4d\t%3d %%" %(c5260_2, safe_div(c5260_2, total_chain2)*100))
	print("  5320: %4d\t%3d %%" %(c5320_2, safe_div(c5320_2, total_chain2)*100))
	print("  5500: %4d\t%3d %%" %(c5500_2, safe_div(c5500_2, total_chain2)*100))
	print("  5600: %4d\t%3d %%" %(c5600_2, safe_div(c5600_2, total_chain2)*100))
	print("  5700: %4d\t%3d %%" %(c5700_2, safe_div(c5700_2, total_chain2)*100))
	print("  5785: %4d\t%3d %%" %(c5785_2, safe_div(c5785_2, total_chain2)*100))
	print("  5852: %4d\t%3d %%" %(c5825_2, safe_div(c5825_2, total_chain2)*100))
	print(" ----------------------")
	print(" Total: %6d" %(total_chain2))

	# write result in file
	report_out = open("print_out.txt", "a+")

	report_out.write( "----------------------\n")
	report_out.write(("    Chain 1 Fails     \n"))
	report_out.write( "----------------------\n")
	report_out.write((" freq.\t\tperc.\n"))
	report_out.write((" 5180: %4d\t%3d %%\n" %(c5180_1, safe_div(c5180_1, total_chain1)*100)))
	report_out.write((" 5260: %4d\t%3d %%\n" %(c5260_1, safe_div(c5260_1, total_chain1)*100)))
	report_out.write((" 5320: %4d\t%3d %%\n" %(c5320_1, safe_div(c5320_1, total_chain1)*100)))
	report_out.write((" 5500: %4d\t%3d %%\n" %(c5500_1, safe_div(c5500_1, total_chain1)*100)))
	report_out.write((" 5600: %4d\t%3d %%\n" %(c5600_1, safe_div(c5600_1, total_chain1)*100)))
	report_out.write((" 5700: %4d\t%3d %%\n" %(c5700_1, safe_div(c5700_1, total_chain1)*100)))
	report_out.write((" 5785: %4d\t%3d %%\n" %(c5785_1, safe_div(c5785_1, total_chain1)*100)))
	report_out.write((" 5852: %4d\t%3d %%\n" %(c5825_1, safe_div(c5825_1, total_chain1)*100)))
	report_out.write( "----------------------\n")
	report_out.write(("Total: %6d\n" %(total_chain1)))	
	report_out.write( "")	
	report_out.write( "----------------------\n")
	report_out.write(("    Chain 2 Fails     \n"))
	report_out.write( "----------------------\n")
	report_out.write((" freq.\t\tperc.\n"))
	report_out.write((" 5180: %4d\t%3d %%\n" %(c5180_2, safe_div(c5180_2, total_chain2)*100)))
	report_out.write((" 5260: %4d\t%3d %%\n" %(c5260_2, safe_div(c5260_2, total_chain2)*100)))
	report_out.write((" 5320: %4d\t%3d %%\n" %(c5320_2, safe_div(c5320_2, total_chain2)*100)))
	report_out.write((" 5500: %4d\t%3d %%\n" %(c5500_2, safe_div(c5500_2, total_chain2)*100)))
	report_out.write((" 5600: %4d\t%3d %%\n" %(c5600_2, safe_div(c5600_2, total_chain2)*100)))
	report_out.write((" 5700: %4d\t%3d %%\n" %(c5700_2, safe_div(c5700_2, total_chain2)*100)))
	report_out.write((" 5785: %4d\t%3d %%\n" %(c5785_2, safe_div(c5785_2, total_chain2)*100)))
	report_out.write((" 5852: %4d\t%3d %%\n" %(c5825_2, safe_div(c5825_2, total_chain2)*100)))
	report_out.write( "----------------------\n")
	report_out.write(("Total: %6d\n" %(total_chain2)))	

	report_out.close()


def count_good_first_run_bad(reports = []):
	first_run_good = 0
	first_run_bad = 0
	second_try = 0
	third_try = 0
	fourth_try = 0

	for report in reports.reportlist:
		if len(report.fail_reports) is 0:
			first_run_good += 1
		else:
			first_run_bad +=1
		
		if len(report.fail_reports) == 1:
			if len(report.pass_reports) != 0:
				second_try += 1
		
		if len(report.fail_reports) == 3:
			if len(report.pass_reports) == 0:
				third_try += 1
		
		if len(report.fail_reports) == 4:
			if len(report.pass_reports) == 0:
				fourth_try += 1

	reports.first_run_bad = first_run_bad
	reports.first_run_good = first_run_good
	reports.second_try = second_try
	reports.third_try = third_try
	reports.fourth_try = fourth_try

	return reports



def main():
	print("")		
	filelist = initiate()	
	reports = build_list_of_reports(filelist)	
	stats, reports = run(reports)
	reports = count_good_first_run_bad(reports)
	split_into_files()
	statistic_fails_per_freq_n_chain()

	
	pass_rate = stats.get_pass_rate()
	fail_rate = stats.get_fail_rate()

	pass_percentage = safe_div(pass_rate*100.0, (pass_rate + fail_rate))
	fail_percentage = safe_div(fail_rate*100.0, (pass_rate + fail_rate))

	# RESULT OUTPUT

	newline=""
	title1="----------------------------------------------"
	title2="               Analysis Summary               "
	title3="----------------------------------------------"
	body1=" Evaluated               \t: %4d boards" % stats.get_num_report_files()
	body6="  - First Run Pass       \t: %4d boards" % reports.first_run_good
	body7="  - First Run Fail       \t: %4d boards (%3.2f%%)" % (reports.first_run_bad, 
		safe_div(reports.first_run_bad, (reports.first_run_bad + reports.first_run_good))*100)
	body8="  - Second Run Pass      \t: %4d boards" % reports.second_try
	body9="  - Second Run Fail      \t: %4d boards (%3.2f%%)" % ((reports.first_run_bad - reports.second_try), 
		safe_div((reports.first_run_bad - reports.second_try), (reports.first_run_bad + reports.first_run_good))*100)
	body2=" Valid EVM Reports       \t: %4d" % (pass_rate + fail_rate)
	body3=" Invalid EVM Reports     \t: %4d" % stats.get_num_discarted()
	body4=" EVM Pass Report         \t: %4d \t%2.2f%%" %(pass_rate, pass_percentage)
	body5=" EVM Fail Report         \t: %4d \t%2.2f%%" %(fail_rate, fail_percentage)

	# console output
	print(newline)
	print(title1)
	print(title2)
	print(title3)
	print(body1)
	print(body6)
	print(body7)
	print(body8)
	print(body9)
	print(newline)
	print(body2)
	print(body3)
	print(body4)
	print(body5)

	# write result to file
	report_out = open("print_out.txt", "a+")
	report_out.write("\n")
	report_out.write(title1 + "\n")
	report_out.write(title2 + "\n")
	report_out.write(title3 + "\n")
	report_out.write(body1 + "\n")
	report_out.write(body6 + "\n")
	report_out.write(body7 + "\n")
	report_out.write(body8 + "\n")
	report_out.write(body9 + "\n")
	report_out.write("\n")
	report_out.write(body2 + "\n")
	report_out.write(body3 + "\n")
	report_out.write("\n")
	for i in reports.invalid_reports:
	    report_out.write("  %s\n" % i)
	report_out.write(body4 + "\n")
	report_out.write(body5 + "\n")
	report_out.close()

main()