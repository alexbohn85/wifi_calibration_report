#---------------------#
# Classes Declaration #
#---------------------#

class Report:
	def __init__(self, board_filelist = [], isValid = False):
		self.board_filelist = board_filelist
		self.fail_reports = []
		self.pass_reports = []		
		self.isValid = isValid

class Reports:
	def __init__(self, reportlist = [], maclist = []):
		self.reportlist = reportlist
		self.maclist = maclist
		self.invalid_reports = []
		self.first_run_bad = 0
		self.first_run_good = 0
		self.second_try = 0
		self.third_try = 0
		self.fourth_try = 0

class Statistics:
	def set_num_report_files(self, stat_num_report_files):
		self.__stat_num_report_files = stat_num_report_files

	def get_num_report_files(self):
		return self.__stat_num_report_files	

	def set_num_discarted(self, stat_num_discarted):
		self.__stat_num_discarted = stat_num_discarted

	def get_num_discarted(self):
		return self.__stat_num_discarted

	def set_pass_rate(self, pass_rate):
		self.__pass_rate = pass_rate

	def get_pass_rate(self):
		return self.__pass_rate

	def set_fail_rate(self, fail_rate):
		self.__fail_rate = fail_rate

	def get_fail_rate(self):
		return self.__fail_rate
